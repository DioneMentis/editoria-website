---
title: Ketty User Guide
subtitle: Your step-by-step guide to using Ketty
version: "2.4"
layout: 'bookindex.njk'
menu: "user guide"
book: ketty
type: cover
order: 800
permalink: "/docs/"
---


This is the user guide for Ketty. Looking for the user guide for Editoria? You can find it [here](/docs/editoria/). 
