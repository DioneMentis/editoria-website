---
title: "Book Collaborators"
class: "component-body  chapter  "
order: 6
---

In the top-right of the Producer page, you will find the ‘Share’ button, this will open the modal to add collaborators.

![](/images/cdbc3f9b6604_medium.png)

Book owner
----------

Only the book owner can mange book collaborators, other book collaborators can only view book permissions.

To add someone as a collaborator to your book:

*   Enter the person’s email address.
    
*   iI the person has already sign-ed up, you’ll see their name to select; alternatively select the invite option.
    
*   Choose the level of access the person should have: _can edit_ or _can view_, then select ‘Share’. An email invitation will be set to the person.
    

![](/images/1c984822987d_medium.png)

Share your book with an exisiting user or invite to join by email

![](/images/e490be6e3ee9_medium.png)

Choose the relevant access level

The book owner can change the type of access or remove a book collaborator using the dropdown next to the person’s name.

![](/images/419dc9c4d92a_medium.png)

Change a collabrators access level or remove their access to the book

Collaborators with edit or view access
--------------------------------------

Collaborators with _edit access_ can change book content or metadata, view export previews, and download PDF or Epub files, but they cannot save export profiles or connect to print-on-demand services. Collaborators with _view access_ cannot change book content or metadata, download PDF or Epub files, save export profiles, or connect to print-on-demand services, but they can view export previews.