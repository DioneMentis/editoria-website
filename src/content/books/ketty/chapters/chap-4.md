---
title: "Create a Book"
class: "component-body  chapter  "
order: 4
---

Start writing your book
-----------------------

If you select ‘Start writing your book’ from the Dashboard, you will be taken directly to the title page where you can enter a title for your book. The title can be edited at a later stage in the Book Metadata modal. Type in your title, and click ‘Continue’.

![](/images/651ff6187c77_medium.png)

Enter a book title

This will take you to the Producer page.

![](/images/1fa15354b3e9_medium.png)

The Producer page

To create a new chapter, click the ‘+’ button in the chapter list on the left of the page, then select the ‘Untitled Chapter’ to open it.

![](/images/2a3df4e29b5a_medium.png)

Create a chapter

Click in the blank editor in the centre of your screen to place your cursor in the chapter; write the title; then select ‘Title’ from the text styling dropdown to apply the style.

![](/images/2e3402197246_medium.png)

Give the chapter a title

![](/images/c7b835572f6f_medium.png)

The Producer page with the first chapter open and the title style applied

More information about writing and editing your book is available in the following chapters.

Import your files
-----------------

If you select ‘Import your files’ from the Dashboard, you will be taken directly to the Import page where you can bulk upload Word docx files. Each file you upload will be a separate chapter in your book. Drag and drop the files from your file browser or click ‘Browse’ to look for files in your file browser. Once you are done, click ‘Continue’. You can reorder chapters, import more chapters, or create new chapters to write into later.

![](/images/19dbe8035174_medium.png)

Import Word docx files

Next, enter a title for your book. The title can be edited at a later stage in the Book Metadata modal. Type in your title, and click ‘Continue’ or hit Enter.

![](/images/a97e75a6fb53_medium.png)

This will take you to the Producer page. The files you uploaded will be displayed in the chapter list on the left.

![](/images/05fa8d6e323e_medium.png)

Imported files listed in the sidebar

Click on one of your chapter titles to open the chapter. You can now edit your chapter content and format it as needed. Formatting is generally preserved from your Word docx files including heading levels and images contained in the files.

![](/images/890fa0301703_medium.png)

Editing chapters after importing files

More information about writing and editing your book is available in the following chapters.