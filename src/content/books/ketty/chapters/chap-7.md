---
title: "Book Metadata"
class: "component-body  chapter  "
order: 7
---

From the Producer page, you can access book metadata by clicking ‘Book Metadata’ in the left sidebar under the title of your book.

![](/images/5f0c66ab2d8f_medium.png)

The information you provide in the Book Metadata modal will be used in auto-generated epub and PDF previews and exports.

Title page
----------

You can change the Title, Subtitle, and Authors.

![](/images/e39c161e459d_medium.png)

Copyright page
--------------

You can add multiple ISBNs to appear on your copyright page, each with its own label. This is useful when creating multiple different exports of your book which may require unique ISBNs.

![](/images/f8856f9a9c49_medium.png)

![](/images/bc38168d7275_medium.png)

You can also add copyright license information. Some license selections include sub-options to choose from.

![](/images/82fd161ac955_medium.png)

When you are done inputting your metadata, click ‘Save’. You can return to the Book Metadata modal at any point to add or update book metadata information.