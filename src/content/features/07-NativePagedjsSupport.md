---
title: "Native Paged.js Support"
image: "ketida-native-pagedjs.svg"
part: 5 
---

Ketty is built by the Coko, the same folks that built Paged.js. Get the best paged media pagination engine you can find anywhere built natively into the best book production platform anywhere.
