---
title: "Microservices Architecture"
image: "ketida-microservices-architecture.svg"
part : 8
---

Ketty has an extensive microservices architecture approach to ensure scalability, it includes an XSweet service to upload docx files, an EPUBchecker service to validate epubs, and Paged.js service to typeset PDFs.
