---
title: "Single Source Book Production"
image: "ketida-single-book-production-2.svg"
part : 1
---

Authors, Editors, Production Staff, Designers all use the same source file - reducing the potential for errors and accelerating the production time. [Single Source Publishing](https://coko.foundation/articles/single-source-publishing.html) at it's best!
