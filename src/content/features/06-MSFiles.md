---
title: "Ingest MS Word Files"
image: "ketida-ingest-ms-word.svg"
part : 6
---

Upload MS Word files into Ketty automatically either one at a time or in a batch.
