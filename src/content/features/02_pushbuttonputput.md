---
title: "Push Button Publishing"
image: "ketida-pagedmedia-pagination-2.svg"
part : 2
---

Push button publishing is baked into Ketty. Typesetting happens in real-time — getting you from written word to final output as quickly as possible. Author proofs created at the push of a button, and final outputs available to you as soon as the work is completed.
