---
title: "Gayathri Doraiswami (India)"
image: "gayathri.jpeg"
part : 8
role: Technical Advisor 
---

Gayathri is the Senior Vice President - Technology & Digital Transformation for publishing services provider Amnet Systems with 23 years of strategic leadership and over a decade experience building and delivering disruptive solutions for global publishing clients across US, Europe, and the UK.
