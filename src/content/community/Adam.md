---
title: "Adam Hyde (New Zealand)"
image: "adam-hyde.jpg"
part : 1
role: Ketty Community Facilitator  
---

Adam brings technical leadership and pioneering insights Coko’s collaborative knowledge production methods and technologies. Adam was awarded the 2015 and 2016 Shuttleworth Fellowship with the goal of building an open source publishing framework.
