---
title: "Jason Colman (USA)"
image: "jason.jpg"
part : 4
role: Publishing Advisor
---

Senior Associate Librarian and Director of Publishing Services at Michigan Publishing. Jason's unit is the home to about 40 open access journals and publishes about 30 books per year across a wide range of disciplines using the open source Fulcrum platform.
