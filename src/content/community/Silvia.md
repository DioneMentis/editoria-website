---
title: "Silva Arapi (Albania)"
image: "silva.jpg"
part: 6
role: Hosting and Deployment Advisor
---

Silva has been active in the Free Software movement since 2015. Silva is one of the co-founders of Cloud68.co, working to build a sustainable company which makes the use of open source digital infrastructure easy and convenient.
