class blank extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  afterPageLayout(page) {
    console.log(page);
    if (page.classList.contains("pagedjs_blank_page")) {
      let uke = document.createElement("div");
      uke.classList.add("uke");
      uke.innerHTML =
        "a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z a b c d e f g h i j k l m n o p q r s t u v w x y z";
      page
        .querySelector(".pagedjs_page_content")
        .insertAdjacentElement("afterbegin", uke);
    }
  }
}

Paged.registerHandlers(blank);
