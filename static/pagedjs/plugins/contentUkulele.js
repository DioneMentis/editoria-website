class removeEmptyContent extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }
  beforeParsed(content) {
    // remove toc

    toSpanInNextElement(content, `h3`, ["title-h3"]);

    content.querySelectorAll("em").forEach((em) => {
      if (em.querySelector("em")) {
      }
      if (em.textContent == "" || em.textContent == " ") {
        em.remove();
      }
    });
  }
}

Paged.registerHandlers(removeEmptyContent);

class cover extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.pageBackground = "";
    this.backgroundText;
  }

  beforeParsed(content) {
    content.querySelectorAll("p").forEach((el) => {
      if (el.innerHTML.length == 0) {
        el.remove();
      }
    });

    // remove figcaption
    content.querySelectorAll(".cover figcaption").forEach((figcaption) => {
      this.backgroundText = figcaption.innerHTML;
      // figcaption.remove();
    });

    // add cover to img
    let cover = content.querySelectorAll(".cover figure");
    cover.forEach((img) => {
      img.classList.add("img-cover");
      img.style.display = "none";
    });
  }

  afterPageLayout(page) {
    if (page.querySelector(".img-cover")) {
      page.style.backgroundImage = `url(${
        page.querySelector(".img-cover img").src
      })`;
      this.pageBackground = `url(${page.querySelector(".img-cover img").src})`;
    }
  }
}

Paged.registerHandlers(cover);

// add ID to each elements

class addID extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
  }

  beforeParsed(content) {
    let tags = [
      "figure",
      "figcaption",
      "img",
      "ol",
      "ul",
      "li",
      "p",
      "img",
      "table",
      "h1",
      "h2",
      "h3",
      "div",
      "aside",
      "td",
      "tr"
    ];
    tags.forEach((tag) => {
      content.querySelectorAll(tag).forEach((el, index) => {
        if (!el.id) {
          el.id = `el-${el.tagName.toLowerCase()}-${index}`;
        }
      });
    });
  }
}

Paged.registerHandlers(addID);

// chapter left+right pages

class chapterintro extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.chapterBackground = "";
  }

  beforeParsed(content) {
    // add cover to img
    let cover = content.querySelectorAll(".chapter header + figure");

    cover.forEach((img) => {
      img.classList.add("img-chap-cover");
      img.style.display = "none";
    });

    content.querySelectorAll(".running-right, .running-left").forEach((rem) => {
      rem.remove();
    });
  }
  afterPageLayout(page) {
    if (page.querySelector(".img-chap-cover")) {
      // console.log(page);
      page.style.backgroundImage = `url(${
        page.querySelector(".img-chap-cover img").src
      })`;
      page.classList.add("pageBackground");
      this.chapterBackground = `url(${
        page.querySelector(".img-chap-cover img").src
      })`;
    } else if (this.chapterBackground.length > 1) {
      page.style.backgroundImage = this.chapterBackground;
      this.chapterBackground = "";
      page.classList.add("pageBackground");
    

    }
  }
}

Paged.registerHandlers(chapterintro);

function toSpanInNextElement(content, element, classNames) {
  if (element.tag != "SPAN") {
    content.querySelectorAll(element).forEach((item) => {
      if (item.nextElementSibling) {
        let createdSpan = document.createElement("span");
        classNames.forEach((classNameSingle) => {
          console.log(item.nextElementSibling);
          createdSpan.classList.add(classNameSingle);
          item.nextElementSibling.classList.add(`wrapper-${classNames}`);
        });
        createdSpan.innerHTML = item.innerHTML;
        item.nextElementSibling.insertAdjacentElement(
          "afterbegin",
          createdSpan
        );
        item.remove();
      }
    });
  }
}
